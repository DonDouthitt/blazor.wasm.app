﻿using Microsoft.AspNetCore.Identity;

namespace Blazor.WASM.App.IdentityServer.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}
